import axios from 'axios'
    
const apiClient = axios.create({  
  baseURL: `https://dump.lwdev.nl/vue-cursus-api`,  
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  getParticipants() {
    return apiClient.get('/deelnemers/')
  },
  getParticipant(id) {
    return apiClient.post('/deelnemerDetails/',{
      id
    }).then((resp)=>{
        return resp.data;
    }).catch((error)=>{
        console.error(error);
    })
  },
  addParticipant(name, department){
    return apiClient.post('/addDeelnemer/',{
      naam: name,
      afdeling: department
    }).then((resp)=>{
      return resp.data;
    }).catch((error)=>{
      console.error(error);
  })
  }
}